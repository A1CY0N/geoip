# Geoip

> Script that allows you to geolocate your IP address

![](logo.png)

Geoip is largely based on this [python script](https://gist.github.com/Sulter/7459117)

## Usage

OS X & Linux:

Download geoip tool :

```sh
git clone https://gitlab.com/A1CY0N/geoip
cd geoip
```

Run geoip :

```sh
foo@computer:~/Documents$ python3 geoip.py
===== 8.8.8.8 =====
[+] Public IP:  8.8.8.8
[+] Country Name:  United States
[+] Country Code:  US
[+] Region Name:  Missouri
[+] Region Code:  MO
[+] City:  Lake Saint Louis
[+] Zip Code:  63367
[+] Latitude:  38.7936
[+] Longitude:  -90.7854
[+] Location link: http://www.openstreetmap.org/#map=11/38.7936/-90.7854
```
## Meta

[@A1CY0N](https://mamot.fr/@a1c0n) – a1cy0n@tutanota.com

[@Sulter](https://github.com/Sulter) – [https://gist.github.com/Sulter/7459117](https://gist.github.com/Sulter/7459117)


Distributed under the MIT license. See ``LICENSE`` for more information.

[https://gitlab.com/A1CY0N](https://gitlab.com/A1CY0N)

## Contributing

1. Fork it (<https://gitlab.com/A1CY0N/geoip>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request
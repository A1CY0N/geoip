#!/usr/bin/env python3
# -*- coding: utf-8 -*- 
import sys
from urllib.request import urlopen
import json
from requests import get
import requests

def get_info(adress):
    api = "https://freegeoip.app/json/" + adress
    try:
        result = urlopen(api).read()
        result = str(result) 
        result = result[2:len(result)-3]
        result = json.loads(result)
    except:
        print("Could not find: ", adress)
        return None

    print("===== "+adress+" =====")
    print("[+] Public IP: ", result["ip"])
    print("[+] Country Name: ", result["country_name"])
    print("[+] Country Code: ", result["country_code"])
    print("[+] Region Name: ", result["region_name"])
    print("[+] Region Code: ", result["region_code"])
    print("[+] City: ", result["city"])
    print("[+] Zip Code: ", result["zip_code"])
    print("[+] Latitude: ", result["latitude"])
    print("[+] Longitude: ", result["longitude"])
    print("[+] Location link: " + "http://www.openstreetmap.org/#map=11/" + str(result["latitude"]) +"/" + str(result["longitude"]))

def showhelp():
    print ("Usage: geoip address [address]...")
    print ("find gelocation of IP addresses and host names, using http://freegeoip.net/")

if __name__ == "__main__":
    inputs = sys.argv
    if "--help" in inputs:
        showhelp()
    else:
        ip = requests.get('https://www.wikipedia.org').headers['X-Client-IP']
        #get_info(ip)
        get_info("8.8.8.8")